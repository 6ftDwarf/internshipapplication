﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof (MapGeneratorScript))]
public class MapGenEditorScript : Editor {

    public override void OnInspectorGUI()
    {
        MapGeneratorScript generator = (MapGeneratorScript)target;

        if (DrawDefaultInspector())
        {
            if (generator.autoUpdate)
            {
                //generator.GenerateMap();
            }
        }

        if (GUILayout.Button("Generate"))
        {
            generator.DrawMapInEditor();
        }
    }

}
