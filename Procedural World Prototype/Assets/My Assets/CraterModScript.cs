﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraterModScript : MonoBehaviour {

    public AnimationCurve craterSlope;
    [Range(1, 200)]
    public int maxRadius;
    [Range(1, 200)]
    public int minRadius;
    [Range(0, 1f)]
    public float minPerlinForImpact;
    
    private Color[] colorMap;
    private List<CraterData> craters = new List<CraterData>();

    public float[,] modHeightmap(float[,] heightMap, int seed, Vector2 mapOffset, float scale)
    {
        
        int mapWidth = heightMap.GetLength(0);
        int mapHeight = heightMap.GetLength(1);
        for (int y = 0 - maxRadius + (int)mapOffset.y; y < mapHeight + maxRadius + (int)mapOffset.y; y++)
        {
            for(int x = 0 - maxRadius + (int)mapOffset.x; x < mapWidth + maxRadius + (int)mapOffset.x; x++)
            {
                if (Mathf.PerlinNoise(x / scale, y / scale) >= minPerlinForImpact)
                {
                    bool foundCraterAtPos = false;
                    for(int i = 0; i < craters.Count; i++)
                    {
                        if (craters[i].pos.x == x && craters[i].pos.y == y)
                        {
                            foundCraterAtPos = true;
                            break;
                        }
                    }
                    if (!foundCraterAtPos)
                    {
                        CraterData crater = new CraterData();
                        crater.pos = new Vector2(x / scale, y / scale);
                        crater.radius = (int)(Mathf.PerlinNoise(x / scale, y / scale) * 10);
                        craters.Add(crater);
                    }
                }
            }
        }

        colorMap = new Color[mapWidth * mapHeight];

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                colorMap[y * mapWidth + x] = new Color(0.5f, 0.5f, 0.5f);
            }
        }

        for (int i = 0; i < craters.Count; i++)
        {

            for (int y = 0; y < mapHeight; y++)
            {
                for(int x = 0; x < mapWidth; x++)
                {
                    float dist = Vector2.Distance(craters[i].pos, new Vector2(x + mapOffset.x, y + mapOffset.y)) / craters[i].radius;
                    if(dist <= 1)
                    {
                        float heightMod = craterSlope.Evaluate(dist) - 1;
                        heightMap[x, y] += heightMod;
                        float color = heightMod + 0.5f;
                        colorMap[y * mapWidth + x] = new Color(color, color, color);
                    }
                }
            }

        }

        return heightMap;
    }

    public Color[] getColors()
    {
        return colorMap;
    }

}

public struct CraterData
{
    public Vector2 pos;
    public int radius;
}