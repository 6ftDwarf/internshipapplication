﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientWindScript : MonoBehaviour {

    public AnimationCurve volumeCurve;
    public GameObject player;

    public float intensity;

    private AudioSource src;

	// Use this for initialization
	void Start () {
        src = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        src.volume = volumeCurve.Evaluate(player.transform.position.y) * intensity;
	}
}
