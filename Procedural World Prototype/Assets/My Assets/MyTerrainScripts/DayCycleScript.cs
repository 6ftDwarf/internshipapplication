﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycleScript : MonoBehaviour {

    public Light sun;
    public GameObject moon;

    public float lightScale;

    private Light moonLight;

    private bool isRunning = false;

	// Use this for initialization
	void Start () {
        moonLight = moon.transform.Find("MoonLight").GetComponent<Light>();
	}

    public void init() { isRunning = true; }
	
	// Update is called once per frame
	void Update () {
        if (isRunning)
        {
            sun.transform.RotateAround(Vector3.zero, Vector3.right, 1f * Time.deltaTime);
            float intensityBase = sun.transform.position.y + 200;
            if (intensityBase > 1000) { intensityBase = 1000; }
            sun.intensity = (intensityBase / 1000) * lightScale;

            moon.transform.RotateAround(Vector3.zero, Vector3.right, 1f * Time.deltaTime);
            moonLight.intensity = ((moon.transform.position.y + 500) / 10000) * lightScale;
        }
    }
}
