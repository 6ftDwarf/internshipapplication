﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitScript : MonoBehaviour {

    public Button exitButton;

    // Use this for initialization
    void Start () {
        exitButton.onClick.AddListener(exit);
    }
	
	private void exit() { Application.Quit(); }
}
