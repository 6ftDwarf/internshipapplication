﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestPlanterScript : MonoBehaviour {

    [Range(0, 100f)]
    public int baseTreeChance;

    public AnimationCurve treeHeightRange;

    [Range(0, 100)]
    public int minRocks;
    [Range(0, 100)]
    public int maxRocks;

    public GameObject broadleafTree;
    public GameObject standingStone;
    public GameObject[] rockArray;

    private System.Random rnd;
    private List<StandingStoneScript> spireList;


    public void init(int seed)
    {
        rnd = new System.Random(seed);
        spireList = new List<StandingStoneScript>();
    }

	public void plantTrees(GameObject terrain)
    {
        TerrainData td = terrain.GetComponent<Terrain>().terrainData;
        Vector3 basePos = terrain.transform.position;

        bool placedLandmark = false;
        int loopTimes = 0;
        while (!placedLandmark && loopTimes < 100) //Without loopTimes<100 will sometimes crash on world gen
        {
            Vector3 landmarkPos = new Vector3(basePos.x + rnd.Next(0, 500), 200, basePos.z + rnd.Next(0, 500));
            landmarkPos = getTerrainPos(landmarkPos);
            if (landmarkPos != Vector3.zero)
            {
                GameObject landmark = Instantiate(standingStone);
                spireList.Add(landmark.GetComponent<StandingStoneScript>());
                RaycastHit hit;
                Ray ray = new Ray(landmarkPos, Vector3.down);
                Physics.Raycast(ray, out hit);
                //landmark.transform.LookAt(hit.normal);
                landmark.transform.Rotate(new Vector3(0, 0, rnd.Next(0, 360)));
                landmark.transform.position = landmarkPos;
                landmark.transform.localScale = new Vector3(rnd.Next(2, 3), rnd.Next(2, 3), rnd.Next(10, 20));
                placedLandmark = true;
            }
            loopTimes++;
        }

        /*
        int numOfRocks = rnd.Next(minRocks, maxRocks);
        for(int i = 0; i < numOfRocks; i++)
        {
            GameObject rock = Instantiate(rockArray[rnd.Next(0, rockArray.Length)]);
            bool placedRock = false;
            while (!placedRock)
            {
                Vector3 rockPos = new Vector3(basePos.x + rnd.Next(0, 500), 200, basePos.z + rnd.Next(0, 500));
                rockPos = getTerrainPos(rockPos);
                if (rockPos != Vector3.zero)
                {
                    rock.transform.position = rockPos;
                    rock.transform.localScale = new Vector3(rnd.Next(5, 10)/10f, rnd.Next(5, 10)/10f, rnd.Next(5, 10)/10f);
                    //rock.transform.Rotate(new Vector3(rnd.Next(0, 360), rnd.Next(0, 360), rnd.Next(0, 360)));
                    placedRock = true;
                }
            }
        }
        */

        for (int y = 0; y < td.heightmapHeight; y += 10)
        {
            for(int x = 0; x < td.heightmapWidth; x += 10)
            {
                float chance = baseTreeChance * treeHeightRange.Evaluate(td.GetHeight(x, y));
                //float chance = baseTreeChance;
                //float chance = td.GetHeight(x, y);
                if(rnd.Next(1, 100) <= chance)
                {
                    
                    Vector3 treePos = new Vector3(basePos.x + x, basePos.y + td.GetHeight(x, y) + 50, basePos.z + y);

                    Vector3 hitPoint = getTerrainPos(treePos);

                    if(hitPoint != Vector3.zero)
                    {
                        GameObject tree = Instantiate(broadleafTree);
                        tree.transform.position = hitPoint;
                        tree.transform.Rotate(new Vector3(0, rnd.Next(0, 360), 0));
                    }
                    
                }
            }
        }
    }

    private Vector3 getTerrainPos (Vector3 basePos)
    {
        RaycastHit hit;
        Ray ray = new Ray(basePos, Vector3.down);
        Physics.Raycast(ray, out hit);
        if (hit.collider != null && hit.collider.name == "TerrainBase(Clone)")
        {
            return hit.point;
        }
        else
        {
            return Vector3.zero;
        }
    }

    public void giveMobList(List<GameObject> mobList)
    {
        foreach(StandingStoneScript s in spireList)
        {
            s.mobList = mobList;
        }
    }

}
