﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {
    
    private System.Random rnd;

    public TerrainGeneratorScript tgen;
    public WeatherScript wscript;
    public DayCycleScript lightScript;
    public Button randomButton;
    public Button seedButton;
    public InputField seedField;
    public GameObject mainMenuPanel;
    public GameObject creationPanel;
    public UnityEngine.UI.Text creationText;
    public GameObject salvationPanel;
    public GameObject stillAlivePanel;
    public UnityEngine.UI.Text seedText;

    public Camera startCamera;
    public GameObject playerCharacter;
    public Camera charCamera;


    // Use this for initialization
    void Start ()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks);
        tgen = GameObject.Find("TerrainMaker").GetComponent<TerrainGeneratorScript>();

        randomButton.onClick.AddListener(onRandomClicked);
        seedButton.onClick.AddListener(onSeedClicked);

        //Cursor.lockState = CursorLockMode.None;
        //Cursor.visible = true;
    }
	
    private void onRandomClicked()
    {
        setupGame(rnd.Next());
    }

    private void onSeedClicked()
    {
        int seed;
        
        if(int.TryParse(seedField.text, out seed))
        {
            //It worked
        }
        else
        {
            seed = 0;
        }

        setupGame(seed);
    }

    private void setupGame(int seed)
    {
        mainMenuPanel.SetActive(false);
        creationPanel.SetActive(true);
        startCamera.GetComponent<FlareLayer>().enabled = false;
        tgen.initWorld(seed, creationText, startCamera);
        wscript.init(seed);
        lightScript.init();
        seedText.text = "Seed: " + seed;
        startCamera.GetComponent<FlareLayer>().enabled = true;
        startCamera.enabled = false;
        startCamera.gameObject.SetActive(false);
        charCamera.enabled = true;
        creationPanel.SetActive(false);
        playerCharacter.SetActive(true);
        salvationPanel.SetActive(true);
        stillAlivePanel.SetActive(true);
    }
	
}
