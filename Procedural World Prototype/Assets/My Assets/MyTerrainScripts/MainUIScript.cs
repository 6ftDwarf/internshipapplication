﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUIScript : MonoBehaviour {

    public int saveToWin;

    public AudioClip winSound;
    public AudioClip loseSound;

    private int creaturesSaved = 0;
    private int creaturesAlive = 25;
    private UnityEngine.UI.Text salvationText;
    private UnityEngine.UI.Text stillAliveText;
    private AudioSource audioSource;

    private bool gameOver = false;

	// Use this for initialization
	void Start () {
        salvationText = GameObject.Find("MainUICanvas").transform.FindChild("SalvationPanel").GetComponentInChildren<UnityEngine.UI.Text>();
        stillAliveText = GameObject.Find("MainUICanvas").transform.FindChild("StillAlivePanel").GetComponentInChildren<UnityEngine.UI.Text>();
        audioSource = GetComponent<AudioSource>();
        salvationText.text = "Creatures saved: " + creaturesSaved + " / " + saveToWin;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void savedPP()
    {
        if (creaturesSaved < saveToWin)
        {
            creaturesSaved++;
            salvationText.text = "Creatures saved: " + creaturesSaved + " / " + saveToWin;
            if (creaturesSaved == saveToWin)
            {
                audioSource.PlayOneShot(winSound);
                salvationText.text = saveToWin + " saved! You win!";
                Destroy(GameObject.Find("Predator(Clone)"));
            }
        }
    }

    public void aliveMM()
    {
        creaturesAlive--;
        if (!gameOver)
        {
            stillAliveText.text = "Creatures alive: " + creaturesAlive;
            if (creaturesAlive < saveToWin)
            {
                gameOver = true;
                stillAliveText.text = "Alive < " + saveToWin + ", GAME OVER";
                audioSource.PlayOneShot(loseSound);
            }
        }
    }
}
