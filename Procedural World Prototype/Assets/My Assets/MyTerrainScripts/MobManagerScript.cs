﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobManagerScript : MonoBehaviour {

    [Range(0, 100)]
    public int minMobCount;
    [Range(0, 100)]
    public int maxMobCount;

    public GameObject mobPrefab;
    public GameObject soulPrefab;
    public GameObject predatorPrefab;
    public GameObject sun;
    public GameObject playerCharacter;

    public AudioClip animalCallSound;
    public AudioClip animalDeathSound;

    private bool hasSpawnedPredator = false;
    private System.Random rnd;

    private List<GameObject> mobList;
	
	public void Init() {
        rnd = new System.Random((int)System.DateTime.Now.Ticks);
        mobList = new List<GameObject>();
        spreadMobs();
	}

    void Update()
    {
        if(!hasSpawnedPredator && sun.transform.position.y <= 0)
        {
            Vector3 plpos = playerCharacter.transform.position; //Player position
            Vector3 prpos = new Vector3(plpos.x, 200, plpos.z + 50); //Predator position

            RaycastHit hit;
            Ray pray = new Ray(prpos, Vector3.down);
            Physics.Raycast(pray, out hit);
            if (hit.collider != null)
            {
                GameObject predator = Instantiate(predatorPrefab);
                predator.GetComponent<PredatorScript>().player = playerCharacter;
                predator.transform.position = new Vector3(hit.point.x, hit.point.y + 1, hit.point.z);
                hasSpawnedPredator = true;
            }
        }
    }
	
	private void spreadMobs()
    {
        int mobCount = rnd.Next(minMobCount, maxMobCount);
        for(int i = 0; i < mobCount; i++)
        {
            GameObject mob = Instantiate(mobPrefab);
            mobList.Add(mob);

            bool placedMob = false;
            while (!placedMob)
            {
                mob.transform.position = new Vector3(rnd.Next(-300, 800), 200, rnd.Next(-300, 800));

                RaycastHit hit;
                Ray ray = new Ray(mob.transform.position, Vector3.down);
                Physics.Raycast(ray, out hit);
                if (hit.collider != null && hit.collider.name != "WaterProDaytime")
                {
                    mob.transform.position = new Vector3(hit.point.x, hit.point.y + 1, hit.point.z);
                    mob.transform.Rotate(new Vector3(0, rnd.Next(0, 360), 0));
                    placedMob = true;
                }
            }

            MobScript script = mob.GetComponent<MobScript>();
            script.seed = rnd.Next();
            script.player = playerCharacter;
            script.soulPrefab = soulPrefab;
            script.callSound = animalCallSound;
            script.deathSound = animalDeathSound;
        }
    }

    public List<GameObject> getMobList() { return mobList; }

}
