﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobScript : MonoBehaviour {

    public int seed = 0;
    public GameObject player;
    public GameObject soulPrefab;
    public GameObject lockedSpire;

    private CharacterController cc;
    private AudioSource audioSource;
    private System.Random rnd;

    private Light soulLight;

    public string mindState;
    private float timeSinceDirChange = 0;
    private float timeSinceSubmerged = 0;
    private float timeSinceLocCheck = 0;
    private Vector3 moveGoal;
    private Vector3 locOnPrevCheck;

    private float gravity = -50.0f;
    private float jumpForce = 0;
    private float walkSpeed = 5f;
    private float waterSpeedDenominator = 3f;

    public bool playerLocked;

    public AudioClip callSound;
    public AudioClip deathSound;

    private float nextSounding = 10f;
    private float nextSoundingWaited = 0;


    // Use this for initialization
    void Start () {
        lockedSpire = null;
        cc = GetComponent<CharacterController>();
        audioSource = GetComponent<AudioSource>();
        rnd = new System.Random(seed);
        nextSounding = rnd.Next(1, 10);
        soulLight = GetComponent<Light>();
        mindState = "wander";
        locOnPrevCheck = transform.position;
        jumpForce = gravity;
        audioSource.pitch = rnd.Next(50, 150) / 100f;
        audioSource.clip = callSound;
    }
	
	// Update is called once per frame
	void Update () {
        if (mindState != "dead")
        {
            if (!playerLocked && Vector3.Distance(transform.position, player.transform.position) < 10)
            {
                playerLocked = true;
                soulLight.intensity = 1;
                GetComponent<MeshRenderer>().material.color = new Color(0.5f, 0.5f, 1.0f);
            }

            nextSoundingWaited += Time.deltaTime;
            if (nextSoundingWaited >= nextSounding)
            {
                nextSounding = rnd.Next(500, 1200) / 100f;
                nextSoundingWaited = 0;
                audioSource.PlayOneShot(callSound);
            }

            timeSinceLocCheck += Time.deltaTime;
            if (timeSinceLocCheck > 1 && transform.position.y > 5)
            {
                timeSinceLocCheck = 0;
                if (Vector3.Distance(transform.position, locOnPrevCheck) < 2 && jumpForce == gravity)
                {
                    jumpForce = 8.0f;
                }
                locOnPrevCheck = transform.position;
            }
            else if (timeSinceLocCheck > 5)
            {
                timeSinceLocCheck = 0;
                if (Vector3.Distance(transform.position, locOnPrevCheck) < 2 && jumpForce == gravity)
                {
                    jumpForce = 8.0f;
                }
                locOnPrevCheck = transform.position;
            }

            if (lockedSpire == null && playerLocked
                && Vector3.Distance(transform.position, player.transform.position) > 30)
            {
                mindState = "return";
                timeSinceSubmerged = 0;
                timeSinceDirChange = 0;
                transform.LookAt(player.transform);
                transform.Rotate(Vector3.right * -transform.eulerAngles.x);
            }
            else
            {
                if (mindState == "wander" || mindState == "captured")
                {
                    timeSinceDirChange += Time.deltaTime;

                    if(lockedSpire != null && Vector3.Distance(transform.position, lockedSpire.transform.position) > 25)
                    {
                        transform.Rotate(new Vector3(0, 180, 0));
                    }

                    if (transform.position.y < 5)
                    {
                        if (timeSinceSubmerged == 0)
                        {
                            timeSinceSubmerged += Time.deltaTime;
                            transform.Rotate(new Vector3(0, 180, 0));
                        }
                        else if (timeSinceSubmerged < 2f)
                        {
                            timeSinceSubmerged += Time.deltaTime;
                        }
                        else
                        {
                            if (mindState == "wander")
                            {
                                transform.Rotate(new Vector3(0, rnd.Next(1, 360), 0));
                                Ray ray = new Ray(new Vector3(transform.position.x, 7, transform.position.z), transform.TransformDirection(Vector3.forward));
                                RaycastHit hit;

                                if (Physics.Raycast(ray, out hit))
                                {
                                    moveGoal = hit.point;
                                }
                            }
                            else if(mindState == "captured")
                            {
                                moveGoal = lockedSpire.transform.position;
                            }
                            mindState = "unsubmerge";
                            timeSinceSubmerged = 0;
                        }
                        timeSinceDirChange = 0;
                    }
                    else if (rnd.Next(1, 1000) < timeSinceDirChange)
                    {
                        transform.Rotate(new Vector3(0, rnd.Next(1, 360), 0));
                        timeSinceDirChange = 0;
                        timeSinceSubmerged = 0;
                    }
                }
                else if (mindState == "unsubmerge")
                {
                    timeSinceDirChange += Time.deltaTime;
                    transform.LookAt(moveGoal);
                    if (Vector3.Distance(transform.position, moveGoal) < 2 || transform.position.y >= 7)
                    {
                        transform.Rotate(-Vector3.right * transform.eulerAngles.x);
                        if (lockedSpire == null)
                        {
                            mindState = "wander";
                        }
                        else
                        {
                            mindState = "captured";
                        }
                    }
                }
                else if (mindState == "return")
                {
                    mindState = "wander";
                }
            }

            Vector3 moveDir = transform.TransformDirection(Vector3.forward) * walkSpeed;
            if(transform.position.y <= 5)
            {
                moveDir.x /= waterSpeedDenominator;
                moveDir.z /= waterSpeedDenominator;
            }
            moveDir.y += jumpForce;

            if (jumpForce != gravity)
            {
                jumpForce -= Time.deltaTime * 10.0f;
                if (jumpForce < gravity)
                {
                    jumpForce = gravity;
                }
            }

            cc.Move(moveDir * Time.deltaTime);
        }
	}

    public void Kill()
    {
        if (mindState != "dead")
        {
            mindState = "dead";
            Destroy(cc);
            GetComponent<Rigidbody>().isKinematic = false;
            BoxCollider collider = transform.gameObject.AddComponent<BoxCollider>();
            GameObject soulStar = Instantiate(soulPrefab);
            soulStar.GetComponent<Light>().intensity = soulLight.intensity;
            Destroy(soulLight);
            soulStar.transform.position = transform.position;
            audioSource.clip = deathSound;
            audioSource.pitch = 0.9f;
            audioSource.PlayOneShot(deathSound);
        }
    }
}
