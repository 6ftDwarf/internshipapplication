﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorScript : MonoBehaviour {

    public GameObject player;
    private MainUIScript uiScript;

    private List<MobScript> animals;
    private MobScript prey = null;

    private CharacterController cc;
    private Light myLight;
    private AudioSource mySound;
    private System.Random rnd;

    private float timeSinceLocCheck = 0;
    private Vector3 locOnPrevCheck;
    private string mindState = "hunt";

    private float gravity = -50.0f;
    private float jumpForce = 0;
    private float walkSpeed = 6f;

    private float pauseTime = 10;
    private float normalLightLevel = 0.75f;
    private float pauseLightLevel = 3;
    private Quaternion rotationOnPause;

    private float nextSounding = 10f;
    private float nextSoundingWaited = 0;

    // Use this for initialization
    void Start ()
    {
        uiScript = GameObject.Find("MainUICanvas").GetComponent<MainUIScript>();

        cc = GetComponent<CharacterController>();
        myLight = GetComponent<Light>();
        mySound = GetComponent<AudioSource>();
        rnd = new System.Random((int)System.DateTime.Now.Ticks);

        locOnPrevCheck = transform.position;
        jumpForce = gravity;

        animals = new List<MobScript>();
        MobScript[] prelist = FindObjectsOfType(typeof(MobScript)) as MobScript[];
        foreach(MobScript script in prelist)
        {
            animals.Add(script);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (mindState != "dead")
        {
            Vector3 moveDir = Vector3.zero;
            /*
            if (mindState == "hunt" && Vector3.Distance(player.transform.position, transform.position) < 15)
            {
                mindState = "flee";
                walkSpeed = 10f;
            }
            */
            if (mindState == "hunt")
            {
                if (prey == null)
                {
                    prey = getClosestPrey();
                }

                nextSoundingWaited += Time.deltaTime;
                if(nextSoundingWaited >= nextSounding)
                {
                    nextSounding = rnd.Next(5, 12);
                    nextSoundingWaited = 0;
                    mySound.pitch = rnd.Next(5, 15) / 10f;
                    mySound.Play();
                }

                if (prey != null)
                {
                    transform.LookAt(prey.transform);
                }

                //LocCheck doubles here as a "should I jump" detector and a "has my prey been saved" detector
                timeSinceLocCheck += Time.deltaTime;
                if (timeSinceLocCheck > 1)
                {
                    timeSinceLocCheck = 0;
                    if (Vector3.Distance(transform.position, locOnPrevCheck) < 1 && jumpForce == gravity)
                    {
                        jumpForce = 8.0f;
                    }
                    locOnPrevCheck = transform.position;

                    if(prey != null && prey.lockedSpire != null)
                    {
                        animals.Remove(prey);
                        prey = getClosestPrey();
                    }
                }

                moveDir = transform.TransformDirection(Vector3.forward) * walkSpeed;
            }
            else if (mindState == "flee")
            {
                if (Vector3.Distance(player.transform.position, transform.position) >= 15)
                {
                    mindState = "hunt";
                    //walkSpeed = 3f;
                }
                else
                {
                    transform.LookAt(2 * transform.position - player.transform.position);

                    timeSinceLocCheck += Time.deltaTime;
                    if (timeSinceLocCheck > 1)
                    {
                        timeSinceLocCheck = 0;
                        if (Vector3.Distance(transform.position, locOnPrevCheck) < 1 && jumpForce == gravity)
                        {
                            jumpForce = 8.0f;
                        }
                        locOnPrevCheck = transform.position;
                    }

                    moveDir = transform.TransformDirection(Vector3.forward) * walkSpeed;
                }
            }
            else if (mindState == "pause")
            {
                if (myLight.intensity > normalLightLevel)
                {
                    myLight.intensity -= ((pauseLightLevel - normalLightLevel) / pauseTime) * Time.deltaTime;
                    transform.rotation = rotationOnPause;
                }
                else
                {
                    mindState = "hunt";
                    //walkSpeed = 3f;
                }
            }

            moveDir.y += jumpForce;

            if (jumpForce != gravity)
            {
                jumpForce -= Time.deltaTime * 10.0f;
                if (jumpForce < gravity)
                {
                    jumpForce = gravity;
                }
            }

            cc.Move(moveDir * Time.deltaTime);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if(mindState != "dead" && col.gameObject.name == "FPSController")
        {
            /*
            mindState = "dead";
            Destroy(cc);
            Destroy(myLight);
            GetComponent<Rigidbody>().isKinematic = false;
            BoxCollider collider = transform.gameObject.AddComponent<BoxCollider>();
            */
        }
        else if(mindState == "hunt" && col.gameObject.name == "TestAnimal(Clone)" && 
            col.gameObject.GetComponent<MobScript>().lockedSpire == null &&
            col.gameObject.GetComponent<MobScript>() == prey)
        {
            col.gameObject.GetComponent<MobScript>().Kill();
            animals.Remove(col.gameObject.GetComponent<MobScript>());
            if (col.gameObject == prey.transform.gameObject)
            {
                prey = getClosestPrey();
                myLight.intensity = pauseLightLevel;
                rotationOnPause = transform.rotation;
                uiScript.aliveMM();
                mindState = "pause";
            }
        }
    }

    private MobScript getClosestPrey()
    {
        MobScript ret = null;
        float closestDist = float.MaxValue;
        foreach(MobScript mob in animals)
        {
            float newDist = Vector3.Distance(transform.position, mob.transform.position);
            if (newDist < closestDist)
            {
                closestDist = newDist;
                ret = mob;
            }
        }
        return ret;
    }
}
