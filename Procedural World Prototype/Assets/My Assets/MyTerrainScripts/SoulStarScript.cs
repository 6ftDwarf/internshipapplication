﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulStarScript : MonoBehaviour {

    public GameObject sun;

    private Light soulLight;
    

	// Use this for initialization
	void Start () {
        soulLight = GetComponent<Light>();
        sun = GameObject.Find("sun");
	}
	
	// Update is called once per frame
	void Update () {
		if(soulLight.range < 2)
        {
            soulLight.range += 0.01f;
        }
        if(transform.position.y < 300)
        {
            transform.Translate(Vector3.up * Time.deltaTime * 30);
        }
        float intensityBase = -sun.transform.position.y;
        if (intensityBase > 1000) { intensityBase = 1000; }
        soulLight.intensity = intensityBase / 1000;
    }
}
