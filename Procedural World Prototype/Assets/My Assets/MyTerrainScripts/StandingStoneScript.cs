﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingStoneScript : MonoBehaviour {

    public List<GameObject> mobList;

    private GameObject predator;
    private GameObject player;
    private GameObject sun;
    private MainUIScript uiScript;

    private Light myLight;
    private AudioSource mySound;
    private AudioSource teleSound;
    private System.Random rnd;

    private float cooldownTime = 0.1f;
    private float cooldownElapsed = 0;

    private float checkWaitTime = 1f;
    private float checkTimeWaited = 0;

    private bool hasSpawnedPredator = false;

	// Use this for initialization
	void Start () {
        predator = null;
        player = GameObject.Find("FPSController");
        sun = GameObject.Find("sun");
        uiScript = GameObject.Find("MainUICanvas").GetComponent<MainUIScript>();

        myLight = GetComponent<Light>();
        mySound = GetComponent<AudioSource>();
        teleSound = transform.Find("TeleportSound").GetComponent<AudioSource>();
        //mySound.Play();
        rnd = new System.Random((int)System.DateTime.Now.Ticks);
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, 0, 1.5f * Time.deltaTime));

        //Get predator once it's spawned. This code will only run once
        if (!hasSpawnedPredator && sun.transform.position.y <= -10)
        {
            predator = GameObject.Find("Predator(Clone)");
            hasSpawnedPredator = true;
        }

        //Teleport flash wait script
        if (myLight.intensity == 3)
        {
            cooldownElapsed += Time.deltaTime;
            if(cooldownElapsed >= cooldownTime)
            {
                myLight.intensity = 0;
                cooldownElapsed = 0;
            }
        }

        //Predator teleport checker
        if (predator != null && hasSpawnedPredator && Vector3.Distance(transform.position, predator.transform.position) < 30)
        {
            Vector3 movePos = (transform.up * 60) + new Vector3(0, 200, 0) + transform.position;
            RaycastHit hit;
            Ray ray = new Ray(movePos, Vector3.down);
            Physics.Raycast(ray, out hit);
            predator.transform.position = hit.point;

            myLight.intensity = 3;
            teleSound.Play();
        }

        //Mob capture
        checkTimeWaited += Time.deltaTime;
        if(checkTimeWaited >= checkWaitTime)
        {
            checkTimeWaited = 0;
            //This attempts to space out capture checks to reduce lag, might be unnecessary
            checkWaitTime = rnd.Next(10, 20) / 10f;

            foreach(GameObject mob in mobList)
            {
                if(Vector3.Distance(mob.transform.position, transform.position) < 25)
                {
                    MobScript script = mob.GetComponent<MobScript>();
                    if(script.playerLocked == true && script.mindState != "dead" && script.lockedSpire == null)
                    {
                        script.mindState = "captured";
                        script.lockedSpire = transform.gameObject;
                        mob.GetComponent<MeshRenderer>().material.color = new Color(0.5f, 1.0f, 1.0f);
                        uiScript.savedPP();
                    }
                }
            }
        }
    }
}
