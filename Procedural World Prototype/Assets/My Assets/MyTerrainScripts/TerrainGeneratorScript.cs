﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneratorScript : MonoBehaviour {

    public GameObject terrainPrefab;

    public int mapTileRadius;

    private int seed;
    public int noiseScale;
    public int perlinOctaves;
    [Range(0, 1.0f)]
    public float persistence;
    public float lacunarity;
    public Vector2 mapOffset;

    public Texture2D[] splatTextures;
    public AnimationCurve[] splatCurves;

    [Range(0, 1.0f)]
    public float heightScale;

    public AnimationCurve heightCurve;

    public MeshCollider waterColliderToKill;

    private System.Random rnd;
    private ForestPlanterScript planterScript;
    private MobManagerScript mobManager;

	// Use this for initialization
	void Start () {
        rnd = new System.Random((int)System.DateTime.Now.Ticks);
        //seed = rnd.Next();
        planterScript = GetComponent<ForestPlanterScript>();
        mobManager = GetComponent<MobManagerScript>();
        /*
        planterScript.init(seed);
        genTerrain();
        mobManager.Init();
        planterScript.giveMobList(mobManager.getMobList());
        Destroy(waterColliderToKill);
        */
    }

    public void initWorld(int seed, UnityEngine.UI.Text textDisplay, Camera menuCamera)
    {
        this.seed = seed;
        planterScript.init(seed);
        //forceRender(Camera.main);
        //menuCamera.Render();
        //Camera.main.Render();
        //Camera.current.Render(); //This one renderes AND makes a NullReferenceException error
        //forceRender(Camera.current);
        genTerrain(textDisplay, menuCamera);
        textDisplay.text = "Placing creatures";
        menuCamera.Render();
        mobManager.Init();
        planterScript.giveMobList(mobManager.getMobList());
        Destroy(waterColliderToKill);
    }
	
	public void genTerrain(UnityEngine.UI.Text textDisplay, Camera menuCamera)
    {
        float[,] falloffMap = FalloffGenerator.genFalloffMap(((mapTileRadius * 2) + 1) * 513, ((mapTileRadius * 2) + 1) * 513);

        for (int y=-mapTileRadius; y <= mapTileRadius; y++)
        {
            for (int x = -mapTileRadius; x <= mapTileRadius; x++)
            {
                Vector2 tileOffset = new Vector2((x * 512) + mapOffset.x, (y * 512) + mapOffset.y);
                textDisplay.text = "Generating tile " + x + ", " + y;
                menuCamera.Render();
                GameObject terrainObj = makeTerrainTile(tileOffset, falloffMap);
                terrainObj.transform.position = new Vector3(-y * 500, 0, x * 500);
                textDisplay.text = "Planting trees on tile " + x + ", " + y;
                menuCamera.Render();
                planterScript.plantTrees(terrainObj);
            }
        }
    }

    private void forceRender(Camera cam)
    {
        try
        {
            cam.Render();
        }
        catch(System.NullReferenceException e) { }
    }
    
    private GameObject makeTerrainTile(Vector2 offset, float[,] falloffMap)
    {
        GameObject terrainObj = Instantiate(terrainPrefab);
        
        Terrain terrain = terrainObj.GetComponent<Terrain>();
        TerrainCollider tc = terrainObj.GetComponent<TerrainCollider>();

        terrain.heightmapPixelError = 1;

        TerrainData td = terrain.terrainData;
        TerrainData newtd = new TerrainData();
        newtd.heightmapResolution = td.heightmapResolution;
        newtd.size = td.size;
        newtd.baseMapResolution = td.baseMapResolution;
        newtd.SetDetailResolution(td.baseMapResolution, 1000); //td.detailResolution is 1024, but will cause errors here
        
        SplatPrototype[] splist = new SplatPrototype[splatTextures.Length];
        for (int i=0; i < splatTextures.Length; i++)
        {
            SplatPrototype sp = new SplatPrototype();
            sp.texture = splatTextures[i];
            splist[i] = sp;
        }
        newtd.splatPrototypes = splist;
        terrain.terrainData = newtd;
        tc.terrainData = newtd;
        
        float[,] heightMap = PerlinNoise.GenNoiseMap(newtd.heightmapHeight, newtd.heightmapWidth, seed, noiseScale, perlinOctaves, persistence, lacunarity, offset, PerlinNoise.NormalizeMode.Global);
        
        for (int y = 0; y < newtd.heightmapHeight; y++)
        {
            for (int x = 0; x < newtd.heightmapWidth; x++)
            {
                //heightMap[y, x] *= heightScale;
                heightMap[x, y] = (heightCurve.Evaluate(heightMap[x, y]) * 1 - falloffMap[(int)offset.x + 513 + x, -(int)offset.y + 513 + y]);
                
            }
        }

        newtd.SetHeights(0, 0, heightMap);

        float[,,] alphaMap = new float[newtd.alphamapWidth, newtd.alphamapHeight, newtd.alphamapLayers];

        // For each point on the alphamap...
        for (int y = 0; y < newtd.alphamapHeight; y++)
        {
            for (int x = 0; x < newtd.alphamapWidth; x++)
            {
                // Get the normalized terrain coordinate that
                // corresponds to the the point.
                //float normX = x * (1.0f / (newtd.alphamapWidth - 1));
                //float normY = y * (1.0f / (newtd.alphamapHeight - 1));
                float normX = x / (newtd.size.x -  12);
                float normY = y / (newtd.size.y - 12);

                // Get the steepness value at the normalized coordinate.
                float angle = newtd.GetSteepness(normY, normX);

                // Steepness is given as an angle, 0..90 degrees. Divide
                // by 90 to get an alpha blending value in the range 0..1.
                float frac = angle / 90.0f;

                // Setup an array to record the mix of texture weights at this point
                float[] splatWeights = new float[splist.Length];

                // CHANGE THE RULES BELOW TO SET THE WEIGHTS OF EACH TEXTURE ON WHATEVER RULES YOU WANT

                for (int i = 0; i < splatWeights.Length; i++)
                {
                    splatWeights[i] = splatCurves[i].Evaluate(newtd.GetHeight(y, x));
                }

                // Sum of all textures weights must add to 1, so calculate normalization factor from sum of weights
                float z = 0;
                for (int i = 0; i < splatWeights.Length; i++)
                {
                    z += splatWeights[i];
                }

                // Loop through each terrain texture
                for (int i = 0; i < splatWeights.Length; i++)
                {

                    // Normalize so that sum of all texture weights = 1
                    splatWeights[i] /= z;

                    // Assign this point to the splatmap array
                    alphaMap[x, y, i] = splatWeights[i];
                }

            }
        }

        newtd.SetAlphamaps(0, 0, alphaMap);

        return terrainObj;
    }

}
