﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherScript : MonoBehaviour {

    public AudioClip[] thunderSounds;

    public GameObject lightningPart;
    private GameObject bolt;

    private LazyClouds clouds;
    private AmbientWindScript wind;
    private DayCycleScript skylight;
    private GameObject lightningObj;

    private WeatherType w_clear;
    private WeatherType w_overcast;
    private WeatherType w_storm;

    private WeatherType w_current;
    private WeatherType w_next;

    private float waitTime;
    private float waited;
    private bool waiting;
    private float transTime;
    private float thunderWait;
    private float thunderWaited;
    private System.Random rnd;

	// Use this for initialization
	void Start () {
        clouds = GameObject.Find("CloudSphere").GetComponent<LazyClouds>();
        wind = GetComponent<AmbientWindScript>();
        skylight = GameObject.Find("DayCycleController").GetComponent<DayCycleScript>();
        lightningObj = GameObject.Find("StarlightLightning");
        //lightningObj.GetComponent<AudioSource>().maxVolume = 10;
        waitTime = 1;
        waiting = true;

        w_clear.intensity = 6;
        w_clear.sharpness = 9;
        w_clear.thickness = 1.7f;
        w_clear.wind = 0.5f;
        w_clear.light = 1f;

        w_overcast.intensity = 4;
        w_overcast.sharpness = 1;
        w_overcast.thickness = 2;
        w_overcast.wind = 0.2f;
        w_overcast.light = 0.6f;

        w_storm.intensity = 1;
        w_storm.sharpness = 1;
        w_storm.thickness = 8;
        w_storm.wind = 1;
        w_storm.light = 0.3f;

        w_current = w_clear;
        clouds.LS_CloudIntensity = w_current.intensity;
        clouds.LS_CloudSharpness = w_current.sharpness;
        clouds.LS_CloudThickness = w_current.thickness;
        wind.intensity = w_current.wind;
        skylight.lightScale = w_current.light;
    }

    public void init(int seed)
    {
        rnd = new System.Random(seed);
        thunderWait = rnd.Next(1, 10);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //High scattering will make solid color
        //Low intensity is darker
        //High sharpness makes very infrequent clouds, low more constant cover, near zero is solid color
        //High thickness looks like overcast or rain, zero is invisible clouds
        if(rnd != null) //only run after init()
        {
            if (waiting) {
                waited += Time.deltaTime;
                thunderWaited += Time.deltaTime;

                if(thunderWaited >= 0.05f) {
                    lightningObj.GetComponent<Light>().intensity = 0.2f;
                    if(bolt != null) { Destroy(bolt); }
                }
                if (w_current.light == w_storm.light) //if in a storm
                {
                    if(thunderWaited >= thunderWait)
                    {
                        thunderWaited = 0;
                        thunderWait = rnd.Next(1, 60);
                        lightningObj.GetComponent<Light>().intensity = 4;
                        lightningObj.GetComponent<AudioSource>().PlayOneShot(thunderSounds[rnd.Next(0, thunderSounds.Length)]);
                        if(rnd.Next(0, 0) == 0)
                        {
                            makeBolt(rnd.Next(-300, 800), 200, rnd.Next(-300, 800));
                        //makeBolt(0, 200, 0);
                        }
                    }
                }

                if (waited >= waitTime)
                {
                    waited = 0;
                    waitTime = rnd.Next(100, 200);
                    waiting = false;

                    int nextWeather = rnd.Next(1, 4);
                    if(nextWeather == 1) { w_next = w_clear; }
                    else if (nextWeather == 2) { w_next = w_overcast; }
                    else if (nextWeather == 3) { w_next = w_storm; }
                    transTime = rnd.Next(100, 160);
                }
                //print("waited " + waited);
            }
            else //changing weathers
            {
                //print("intensity += " + ((w_current.intensity - w_next.intensity) / transTime) * Time.deltaTime);
                //print("intensity " + clouds.LS_CloudIntensity + " sharpness " + clouds.LS_CloudSharpness
                     //+ " thickness " + clouds.LS_CloudThickness);
                clouds.LS_CloudIntensity += ((w_next.intensity - w_current.intensity) / transTime) * Time.deltaTime;
                clouds.LS_CloudSharpness += ((w_next.sharpness - w_current.sharpness) / transTime) * Time.deltaTime;
                clouds.LS_CloudThickness += ((w_next.thickness - w_current.thickness) / transTime) * Time.deltaTime;
                wind.intensity += ((w_next.wind - w_current.wind) / transTime) * Time.deltaTime;
                skylight.lightScale += ((w_next.light - w_current.light) / transTime) * Time.deltaTime;
                waited += Time.deltaTime;

                if(waited >= transTime)
                {
                    waited = 0;
                    w_current = w_next;
                    waiting = true;
                }
            }
        }
	}

    private void makeBolt(float x, float y, float z)
    {
        bolt = new GameObject();

        GameObject starter = Instantiate(lightningPart);
        starter.transform.position = new Vector3(x, y, z);
        starter.transform.Rotate(new Vector3(0, 0, 270));
        starter.transform.parent = bolt.transform;

        while(starter.transform.FindChild("LightningEnd").transform.position.y > 0)
        {
            GameObject next = Instantiate(lightningPart);
            next.transform.position = starter.transform.FindChild("LightningEnd").transform.position;
            next.transform.Rotate(new Vector3(0, rnd.Next(-30, 30), rnd.Next(-30, 30) + 270));
            next.transform.parent = bolt.transform;
            starter = next;
        }

    }

    struct WeatherType
    {
        public float intensity;
        public float sharpness;
        public float thickness;
        public float wind; //0 to 1
        public float light; //0 to 1
    };
}
