﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindZoneScript : MonoBehaviour {

    private WindZone zone;

    private bool increasing = false;

	// Use this for initialization
	void Start () {
        zone = GetComponent<WindZone>();
	}
	
	// Update is called once per frame
	void Update () {

        if (increasing)
        {
            //zone.windMain += 0.001f * Time.deltaTime;
        }
        else
        {
            //zone.windMain -= 0.001f * Time.deltaTime;
        }

		if(zone.windMain >= 0.3)
        {
            increasing = false;
        }
        else if(zone.windMain <= 0)
        {
            increasing = true;
        }
        print(zone.windMain);
	}
}
