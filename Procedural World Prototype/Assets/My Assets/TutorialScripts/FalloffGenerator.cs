﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FalloffGenerator {

	public static float[,] genFalloffMap(int width, int height)
    {
        float[,] map = new float[width, height];

        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                float xVal = x / (float)width * 2 - 1;
                float yVal = y / (float)height * 2 - 1;

                float value = Mathf.Max(Mathf.Abs(xVal), Mathf.Abs(yVal));
                map[x, y] = evaluate(value);
            }
        }

        return map;
    }

    static float evaluate(float value)
    {
        float a = 3;
        float b = 2.2f;

        return Mathf.Pow(value, a) / (Mathf.Pow(value, a) + Mathf.Pow(b - b * value, a));
    }

}
