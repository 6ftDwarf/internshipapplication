﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using UnityEngine;

public class MapGeneratorScript : MonoBehaviour {

    public enum DrawMode {NoiseMap, ColorMap, Mesh, FalloffMap};
    public DrawMode drawMode;

    public PerlinNoise.NormalizeMode normalizeMode;

    public const int mapChunkSize = 241; //This is both width and height, and size of mesh is actually var-1
    [Range(1,6)]
    public int editorPreviewLOD; //or LOD

    //public int mapWidth;
   // public int mapHeight;
    public float noiseScale;

    public int octaves;
    [Range(0,1)]
    public float persistence;
    public float lacunarity;

    public int seed;
    public Vector2 mapOffset;

    public bool useFalloffMap;

    public float mapHeightMultiplier;
    public AnimationCurve mapHeightCurve;

    public bool autoUpdate;

    public TerrainType[] ttypes;

    public bool useCraters;
    public CraterModScript craterMod;

    private float[,] falloffMap;

    private Queue<MapThreadInfo<MapData>> mapDataThreadInfoQueue = new Queue<MapThreadInfo<MapData>>();
    private Queue<MapThreadInfo<MeshData>> meshDataThreadInfoQueue = new Queue<MapThreadInfo<MeshData>>();

    void Awake()
    {
        falloffMap = FalloffGenerator.genFalloffMap(mapChunkSize, mapChunkSize);
        drawMode = DrawMode.Mesh;
        System.Random rnd = new System.Random((int)System.DateTime.Now.Ticks);
        seed = rnd.Next(-100000, 100000);
        DrawMapInEditor();
    }

    public void DrawMapInEditor()
    {
        MapData mapData = GenerateMapData(Vector2.zero);

        MapDisplayScript display = FindObjectOfType<MapDisplayScript>();
        if (drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(mapData.heightMap));
        }
        else if (drawMode == DrawMode.ColorMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColorMap(mapData.colorMap, mapChunkSize, mapChunkSize));
        }
        else if (drawMode == DrawMode.Mesh)
        {
            display.DrawMesh(MeshGenerator.genTerrainMesh(mapData.heightMap, mapHeightMultiplier, mapHeightCurve, editorPreviewLOD), TextureGenerator.TextureFromColorMap(mapData.colorMap, mapChunkSize, mapChunkSize));
        }
        else if (drawMode == DrawMode.FalloffMap)
        {
            falloffMap = FalloffGenerator.genFalloffMap(mapChunkSize, mapChunkSize);
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(falloffMap));
        }
    }

    public void RequestMapData(Vector2 center, Action<MapData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MapDataThread(center, callback);
        };

        new Thread(threadStart).Start();
    }

    public void RequestMeshData(MapData mapData, int lod, Action<MeshData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MeshDataThread(mapData, lod, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MapDataThread(Vector2 center, Action<MapData> callback)
    {
        MapData mapData = GenerateMapData(center);
        lock (mapDataThreadInfoQueue)
        {
            mapDataThreadInfoQueue.Enqueue(new MapThreadInfo<MapData>(callback, mapData));
        }
    }

    private void MeshDataThread(MapData mapData, int lod, Action<MeshData> callback)
    {
        MeshData meshData = MeshGenerator.genTerrainMesh(mapData.heightMap, mapHeightMultiplier, mapHeightCurve, lod);
        lock (meshDataThreadInfoQueue)
        {
            meshDataThreadInfoQueue.Enqueue(new MapThreadInfo<MeshData>(callback, meshData));
        }
    }

    void Update()
    {
        if(mapDataThreadInfoQueue.Count > 0)
        {
            for(int i = 0; i < mapDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<MapData> threadInfo = mapDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
        if (meshDataThreadInfoQueue.Count > 0)
        {
            for (int i = 0; i < meshDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<MeshData> threadInfo = meshDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
    }

    private MapData GenerateMapData(Vector2 center)
    {
        float[,] noiseMap = PerlinNoise.GenNoiseMap(mapChunkSize, mapChunkSize, seed, noiseScale, octaves, persistence, lacunarity, center + mapOffset, normalizeMode);

        if (useCraters)
        {
            noiseMap = craterMod.modHeightmap(noiseMap, seed, center, noiseScale);
        }

        Color[] colorMap;
        if (!useCraters)
        {
            colorMap = new Color[mapChunkSize * mapChunkSize];
        }
        else
        {
            colorMap = craterMod.getColors();
        }
        

        for(int y = 0; y < mapChunkSize; y++)
        {
            for(int x = 0; x < mapChunkSize; x++)
            {
                if (useFalloffMap)
                {
                    noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y] - falloffMap[x, y]);
                }
                float currentHeight = noiseMap[x, y];
                if (!useCraters)
                {
                    for (int i = 0; i < ttypes.Length; i++)
                    {
                        if (currentHeight >= ttypes[i].height)
                        {
                            colorMap[y * mapChunkSize + x] = ttypes[i].color;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                
            }
        }

        return new MapData(noiseMap, colorMap);

    }

    void OnValidate()
    {
        if(lacunarity < 1)
        {
            lacunarity = 1;
        }
        if(octaves < 1)
        {
            octaves = 1;
        }
        
        falloffMap = FalloffGenerator.genFalloffMap(mapChunkSize, mapChunkSize); //Just in case mapWidth or mapHeight changed
    }

    private struct MapThreadInfo<T>
    {
        public readonly Action<T> callback;
        public readonly T parameter;

        public MapThreadInfo(Action<T> callback, T parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }

}

[System.Serializable]
public struct TerrainType
{
    public string name;
    public float height;
    public Color color;
}

public struct MapData
{
    public readonly float[,] heightMap;
    public readonly Color[] colorMap;

    public MapData (float[,] heightMap, Color[] colorMap)
    {
        this.heightMap = heightMap;
        this.colorMap = colorMap;
    }
}