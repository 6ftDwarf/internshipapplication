﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerlinNoise  {

    public enum NormalizeMode { Local, Global };

    public static float[,] GenNoiseMap(int width, int height, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset, NormalizeMode normalizeMode)
    {
        float[,] noiseMap = new float[width, height];

        System.Random rnd = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];
        float amplitude = 1;
        float frequency = 1;

        float maxPossibleHeight = 0;

        for (int i = 0; i < octaves; i++)
        {
            float offsetX = rnd.Next(-100000, 100000) + offset.x;
            float offsetY = rnd.Next(-100000, 100000) - offset.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);

            maxPossibleHeight += amplitude;
            amplitude *= persistence;
        }

        if(scale <= 0)
        {
            scale = 0.0001f;
        }

        float maxLocalNoiseVal = float.MinValue;
        float minLocalNoiseVal = float.MaxValue;

        float halfWidth = width / 2f;
        float halfHeight = height / 2f;

        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                amplitude = 1;
                frequency = 1;
                float noiseVal = 0;

                for (int i = 0; i < octaves; i++)
                {
                    float sampleX = (x- halfWidth + octaveOffsets[i].x) / scale * frequency;
                    float sampleY = (y- halfHeight + octaveOffsets[i].y) / scale * frequency;

                    float prlnVal = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseVal += prlnVal * amplitude;

                    amplitude *= persistence;
                    frequency *= lacunarity;
                }

                if(noiseVal > maxLocalNoiseVal)
                {
                    maxLocalNoiseVal = noiseVal;
                }
                else if(noiseVal < minLocalNoiseVal)
                {
                    minLocalNoiseVal = noiseVal;
                }
                noiseMap[x, y] = noiseVal;
            }
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if(normalizeMode == NormalizeMode.Local)
                {
                    noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseVal, maxLocalNoiseVal, noiseMap[x, y]);
                }
                else if(normalizeMode == NormalizeMode.Global)
                {
                    float normalizedHeight = (noiseMap[x, y] + 1) / (2f * maxPossibleHeight / 2f);
                    noiseMap[x, y] = normalizedHeight;
                }
            }
        }

        return noiseMap;
    }

}
