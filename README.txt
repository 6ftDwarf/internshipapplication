This Unity project was made as a part of my internship application to Survios.
This project was made without the use of Git, and later uploaded to BitBucket.
It uses C#, rather than JavaScript, for the scripts.
Some of the scripts and many of the non-code assets are from the Unity Asset store.
My scripts are in the folder Assets\My Assets\MyTerrainScripts.